/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../..//test-helper.js')
const util = require('util')

const testEachFieldAsNull = util.promisify((req, callback) => {
  const fields = Object.keys(req.body)
  async function nextField () {
    if (fields.length === 0) {
      return callback()
    }
    const field = fields.shift()
    const value = req.body[field]
    req.body[field] = null
    const res = TestHelper.createResponse()
    res.end = (str) => {
      req.body[field] = value
      const doc = TestHelper.extractDoc(str)
      const messageContainer = doc.getElementById('message-container')
      const message = messageContainer.child[0]
      assert.strictEqual(message.attr.template, `invalid-${field}`)
      return nextField()
    }
    return req.route.api.post(req, res)
  }
  return nextField()
})

const testRequiredFieldInputsExist = util.promisify((req, callback) => {
  const res = TestHelper.createResponse()
  res.end = (str) => {
    const doc = TestHelper.extractDoc(str)
    for (const pathAndField of req.data.fieldsNeeded) {
      if (pathAndField === 'external_account' ||
          pathAndField === 'legal_entity.additional_owners' ||
          pathAndField === 'legal_entity.type' ||
          pathAndField === 'tos_acceptance.date' ||
          pathAndField === 'tos_acceptance.ip' ||
          pathAndField === 'tos_acceptance.user_agent' ||
          pathAndField === 'legal_entity.verification.document') {
        continue
      }
      const field = pathAndField.split('.').pop()
      let inputName
      switch (pathAndField) {
        case 'legal_entity.address.line1':
        case 'legal_entity.address.line2':
        case 'legal_entity.address.state':
        case 'legal_entity.address.city':
        case 'legal_entity.address.country':
        case 'legal_entity.address.postal_code':
        case 'legal_entity.address_kana.town':
        case 'legal_entity.address_kana.line1':
        case 'legal_entity.address_kana.line2':
        case 'legal_entity.address_kana.state':
        case 'legal_entity.address_kana.city':
        case 'legal_entity.address_kana.country':
        case 'legal_entity.address_kana.postal_code':
        case 'legal_entity.address_kanji.town':
        case 'legal_entity.address_kanji.line1':
        case 'legal_entity.address_kanji.line2':
        case 'legal_entity.address_kanji.state':
        case 'legal_entity.address_kanji.city':
        case 'legal_entity.address_kanji.country':
        case 'legal_entity.address_kanji.postal_code':
          inputName = `company_${field}`
          break
        case 'legal_entity.personal_address.line1':
        case 'legal_entity.personal_address.line2':
        case 'legal_entity.personal_address.state':
        case 'legal_entity.personal_address.city':
        case 'legal_entity.personal_address.country':
        case 'legal_entity.personal_address.postal_code':
        case 'legal_entity.personal_address_kana.town':
        case 'legal_entity.personal_address_kana.line1':
        case 'legal_entity.personal_address_kana.line2':
        case 'legal_entity.personal_address_kana.state':
        case 'legal_entity.personal_address_kana.city':
        case 'legal_entity.personal_address_kana.country':
        case 'legal_entity.personal_address_kana.postal_code':
        case 'legal_entity.personal_address_kanji.town':
        case 'legal_entity.personal_address_kanji.line1':
        case 'legal_entity.personal_address_kanji.line2':
        case 'legal_entity.personal_address_kanji.state':
        case 'legal_entity.personal_address_kanji.city':
        case 'legal_entity.personal_address_kanji.country':
        case 'legal_entity.personal_address_kanji.postal_code':
          inputName = `personal_${field}`
          break
        default:
          inputName = field
          break
      }
      if (req.data.applicationCountry.id === 'JP') {
        if (pathAndField.indexOf('kana') > -1 && !pathAndField.endsWith('_kana')) {
          inputName += '_kana'
        } else if (pathAndField.indexOf('kanji') > -1 && !pathAndField.endsWith('_kanji')) {
          inputName += '_kanji'
        }
      }
      const input = doc.getElementById(inputName)
      if (input.attr.name === 'personal_state' || input.attr.name === 'personal_country' ||
        input.attr.name === 'company_state' || input.attr.name === 'company_country') {
        assert.strictEqual(input.tag, 'select')
      } else if (input.attr.id === 'gender') {
        assert.strictEqual(input.tag, 'div')
      } else {
        assert.strictEqual(input.tag, 'input')
      }
    }
    return callback()
  }
  return req.route.api.get(req, res)
})

describe(`/account/connect/edit-company-registration`, async () => {
  describe('EditCompanyRegistration#BEFORE', () => {
    it('should reject invalid registration', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=invalid`, 'POST')
      req.account = user.account
      req.session = user.session
      let errorMessage
      try {
        await req.route.api.before(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-stripeid')
    })

    it('should reject individual registration', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'US', city: 'New York City', state: 'NY', postal_code: '10001', personal_id_number: '000000000', line1: 'First Street', day: '1', month: '1', year: '1950', ssn_last_4: '0000' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      let errorMessage
      try {
        await req.route.api.before(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-stripe-account')
    })

    it('should reject invalid personal address country', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'US', city: 'New York City', personal_city: 'New York City', postal_code: '10001', personal_id_number: '000000000', line1: 'First Street', day: '1', month: '1', year: '1950', company_city: 'New York City', company_state: 'New York', company_line1: 'First Street', company_postal_code: '10001', ssn_last_4: '0000' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_country: 'invalid'
      }
      await req.route.api.before(req)
      assert.strictEqual(req.error, 'invalid-personal_country')
    })

    it('should reject invalid company address country', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'US', city: 'New York City', personal_city: 'New York City', postal_code: '10001', personal_id_number: '000000000', line1: 'First Street', day: '1', month: '1', year: '1950', company_city: 'New York City', company_state: 'New York', company_line1: 'First Street', company_postal_code: '10001', ssn_last_4: '0000' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        company_country: 'invalid'
      }
      await req.route.api.before(req)
      assert.strictEqual(req.error, 'invalid-company_country')
    })

    it('should bind application CountrySpec to req', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'AU', day: '1', month: '1', year: '1950', company_city: 'Brisbane', company_country: 'AU', company_line1: 'First Street', company_postal_code: '4000', company_state: 'QLD', personal_postal_code: '4000', personal_city: 'Brisbane', personal_state: 'QLD', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      await req.route.api.before(req)
      assert.strictEqual(req.data.applicationCountry.id, 'AU')
    })

    it('should bind personal address country to req', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'AU', day: '1', month: '1', year: '1950', company_city: 'Brisbane', company_country: 'AU', company_line1: 'First Street', company_postal_code: '4000', company_state: 'QLD', personal_postal_code: '4000', personal_city: 'Brisbane', personal_state: 'QLD', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_country: 'CA'
      }
      await req.route.api.before(req)
      assert.strictEqual(req.data.personalAddressCountry.code, req.body.personal_country)
    })

    it('should bind company address country to req', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'AU', day: '1', month: '1', year: '1950', company_city: 'Brisbane', company_country: 'AU', company_line1: 'First Street', company_postal_code: '4000', company_state: 'QLD', personal_postal_code: '4000', personal_city: 'Brisbane', personal_state: 'QLD', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      req.body = {
        company_country: 'GB'
      }
      await req.route.api.before(req)
      assert.strictEqual(req.data.companyAddressCountry.code, req.body.company_country)
    })
  })

  describe('EditCompanyRegistration#GET', () => {
    it('should present the form', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'AU', day: '1', month: '1', year: '1950', company_city: 'Brisbane', company_country: 'AU', company_line1: 'First Street', company_postal_code: '4000', company_state: 'QLD', personal_postal_code: '4000', personal_city: 'Brisbane', personal_state: 'QLD', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        assert.strictEqual(doc.getElementById('submit-form').tag, 'form')
        assert.strictEqual(doc.getElementById('submit-button').tag, 'button')
      }
      return req.route.api.get(req, res)
    })

    it('should have AT-required fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'AT', day: '1', month: '1', year: '1950', company_city: 'Vienna', company_postal_code: '1020', company_line1: 'First Street', personal_city: 'Vienna', personal_line1: 'First Street', personal_postal_code: '1020' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      await testRequiredFieldInputsExist(req)
    })

    it('should have AU-required fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'AU', day: '1', month: '1', year: '1950', company_city: 'Brisbane', company_country: 'AU', company_line1: 'First Street', company_postal_code: '4000', company_state: 'QLD', personal_postal_code: '4000', personal_city: 'Brisbane', personal_state: 'QLD', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      await testRequiredFieldInputsExist(req)
    })

    it('should have BE-required fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'BE', day: '1', month: '1', year: '1950', company_city: 'Brussels', company_postal_code: '1020', company_line1: 'First street', personal_city: 'Brussels', personal_postal_code: '1020', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      await testRequiredFieldInputsExist(req)
    })

    it('should have CA-required fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'CA', day: '1', month: '1', year: '1950', company_city: 'Vancouver', company_state: 'BC', company_postal_code: 'V7G 0A1', company_line1: 'First Street', personal_city: 'Vancouver', personal_state: 'BC', personal_postal_code: 'V7G 0A1', personal_line1: 'First Street', personal_id_number: '000000000' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      await testRequiredFieldInputsExist(req)
    })

    it('should have CH-required fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'CH', day: '1', month: '1', year: '1950', company_city: 'Bern', company_postal_code: '1020', company_line1: 'First Street', personal_city: 'Bern', personal_postal_code: '1020', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      await testRequiredFieldInputsExist(req)
    })

    it('should have DE-required fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'DE', day: '1', month: '1', year: '1950', company_city: 'Berlin', company_line1: 'First Street', company_postal_code: '01067', personal_city: 'Berlin', personal_line1: 'First Street', personal_postal_code: '01067' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      await testRequiredFieldInputsExist(req)
    })

    it('should have DK-required fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'DK', day: '1', month: '1', year: '1950', company_city: 'Copenhagen', company_postal_code: '2300', company_line1: 'First Street', personal_city: 'Copenhagen', personal_postal_code: '2300', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      await testRequiredFieldInputsExist(req)
    })

    it('should have ES-required fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'ES', day: '1', month: '1', year: '1950', company_city: 'Madrid', company_postal_code: '03179', company_line1: 'First Street', personal_city: 'Madrid', personal_postal_code: '03179', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      await testRequiredFieldInputsExist(req)
    })

    it('should have FI-required fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'FI', day: '1', month: '1', year: '1950', company_city: 'Helsinki', company_postal_code: '00990', company_line1: 'First Street', personal_city: 'Helsinki', personal_postal_code: '00990', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      await testRequiredFieldInputsExist(req)
    })

    it('should have FR-required fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'FR', day: '1', month: '1', year: '1950', company_city: 'Paris', company_postal_code: '75001', company_line1: 'First Street', personal_city: 'Paris', personal_postal_code: '75001', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      await testRequiredFieldInputsExist(req)
    })

    it('should have GB-required fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'GB', day: '1', month: '1', year: '1950', company_city: 'London', company_postal_code: 'EC1A 1AA', company_line1: 'First Street', personal_city: 'London', personal_postal_code: 'EC1A 1AA', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      await testRequiredFieldInputsExist(req)
    })

    it('should have HK-required fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'HK', personal_id_number: '7', line1: 'First Street', day: '1', month: '1', year: '1950', company_city: 'Hong Kong', personal_city: 'Hong Kong', company_line1: 'First Street', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      await testRequiredFieldInputsExist(req)
    })

    it('should have IE-required fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'IE', day: '1', month: '1', year: '1950', company_city: 'Dublin', company_line1: 'First Street', personal_city: 'Dublin', personal_line1: 'First Street', company_state: 'Dublin', personal_state: 'Dublin' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      await testRequiredFieldInputsExist(req)
    })

    it('should have IT-required fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'IT', day: '1', month: '1', year: '1950', company_city: 'Rome', company_postal_code: '10001', company_line1: 'First Street', personal_city: 'Rome', personal_line1: 'First Street', personal_postal_code: '00010' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      await testRequiredFieldInputsExist(req)
    })

    it('should have JP-required fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', business_name_kana: user.profile.firstName + '\'s company', business_name_kanji: user.profile.firstName + '\'s company', country: 'JP', day: '1', month: '1', year: '1950', gender: 'female', first_name_kana: 'ﾄｳｷﾖｳﾄ', last_name_kana: 'ﾄｳｷﾖｳﾄ', first_name_kanji: '東京都', last_name_kanji: '東京都', phone_number: '0859-076500', personal_postal_code_kana: '1500001', personal_state_kana: 'ﾄｳｷﾖｳﾄ', personal_city_kana: 'ｼﾌﾞﾔ', personal_town_kana: 'ｼﾞﾝｸﾞｳﾏｴ 3-', personal_line1_kana: '27-15', personal_postal_code_kanji: '１５００００１', personal_state_kanji: '東京都', personal_city_kanji: '渋谷区', personal_town_kanji: '神宮前　３丁目', personal_line1_kanji: '２７－１５', company_postal_code_kana: '1500001', company_state_kana: 'ﾄｳｷﾖｳﾄ', company_city_kana: 'ｼﾌﾞﾔ', company_town_kana: 'ｼﾞﾝｸﾞｳﾏｴ 3-', company_line1_kana: '27-15', company_postal_code_kanji: '１５００００１', company_state_kanji: '東京都', company_city_kanji: '渋谷区', company_town_kanji: '神宮前　３丁目', company_line1_kanji: '２７－１５' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      await testRequiredFieldInputsExist(req)
    })

    it('should have LU-required fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'LU', day: '1', month: '1', year: '1950', company_city: 'Luxemburg', company_postal_code: '1623', company_line1: 'First Street', personal_city: 'Luxemburg', personal_postal_code: '1623', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      await testRequiredFieldInputsExist(req)
    })

    it('should have NL-required fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'NL', day: '1', month: '1', year: '1950', company_city: 'Amsterdam', company_postal_code: '1071 JA', company_line1: 'First Street', personal_city: 'Amsterdam', personal_postal_code: '1071 JA', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      await testRequiredFieldInputsExist(req)
    })

    it('should have NO-required fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'NO', day: '1', month: '1', year: '1950', company_city: 'Oslo', company_postal_code: '0001', company_line1: 'First Street', personal_city: 'Oslo', personal_postal_code: '0001', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      await testRequiredFieldInputsExist(req)
    })

    it('should have NZ-required fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'NZ', day: '1', month: '1', year: '1950', company_city: 'Auckland', company_postal_code: '6011', company_line1: 'First Street', personal_city: 'Auckland', personal_postal_code: '6011', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      await testRequiredFieldInputsExist(req)
    })

    it('should have PT-required fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'PT', day: '1', month: '1', year: '1950', company_city: 'Lisbon', company_postal_code: '4520', company_line1: 'First Street', personal_city: 'Lisbon', personal_postal_code: '4520', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      await testRequiredFieldInputsExist(req)
    })

    it('should have SE-required fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'SE', day: '1', month: '1', year: '1950', company_city: 'Stockholm', company_postal_code: '00150', company_line1: 'First Street', personal_city: 'Stockholm', personal_postal_code: '00150', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      await testRequiredFieldInputsExist(req)
    })

    // these tests only work if your Stripe account is SG
    // it('should have SG-required fields', async () => {
    //   const user = await TestHelper.createUser()
    //   await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'SG', postal_code: '339696', line1: 'First Street', day: '1', month: '1', year: '1950', company_line1: 'First Street', company_postal_code: '339696', personal_line1: 'First Street', personal_postal_code: '339696', personal_id_number: '000000000' })
    //   const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
    //   req.account = user.account
    //   req.session = user.session
    //   await testRequiredFieldInputsExist(req)
    // })

    it('should have US-required fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'US', city: 'New York City', personal_city: 'New York City', postal_code: '10001', personal_id_number: '000000000', line1: 'First Street', day: '1', month: '1', year: '1950', company_city: 'New York City', company_state: 'New York', company_line1: 'First Street', company_postal_code: '10001', ssn_last_4: '0000' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'GET')
      req.account = user.account
      req.session = user.session
      await testRequiredFieldInputsExist(req)
    })
  })

  describe('EditCompanyRegistration#POST', () => {
    it('should refresh and load states for posted company address', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'AU', day: '1', month: '1', year: '1950', company_city: 'Brisbane', company_country: 'AU', company_line1: 'First Street', company_postal_code: '4000', company_state: 'QLD', personal_postal_code: '4000', personal_city: 'Brisbane', personal_state: 'QLD', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        company_country: 'AU',
        refresh: 'true'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        assert.strictEqual(doc.getElementById('submit-form').tag, 'form')
        assert.strictEqual(doc.getElementById('submit-button').tag, 'button')
        const stateField = doc.getElementById('company_state')
        assert.strictEqual(stateField.toString().indexOf('QLD') > -1, true)
      }
      return req.route.api.post(req, res)
    })

    it('should reject AT invalid fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'AT', day: '1', month: '1', year: '1950', company_city: 'Vienna', company_postal_code: '1020', company_line1: 'First Street', personal_city: 'Vienna', personal_line1: 'First Street', personal_postal_code: '1020' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.file = await TestHelper.uploadFile(user)
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Vienna',
        personal_line1: 'Address First Line',
        personal_postal_code: '1020',
        company_city: 'Vienna',
        company_line1: 'Address First Line',
        company_postal_code: '1020',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it('should submit AT information', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'AT', day: '1', month: '1', year: '1950', company_city: 'Vienna', company_postal_code: '1020', company_line1: 'First Street', personal_city: 'Vienna', personal_line1: 'First Street', personal_postal_code: '1020' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_country: 'AT',
        personal_city: 'Vienna',
        personal_line1: 'Address First Line',
        personal_postal_code: '1020',
        company_country: 'AT',
        company_city: 'Vienna',
        company_line1: 'Address First Line',
        company_postal_code: '1020',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.post(req, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should reject AU invalid fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'AU', day: '1', month: '1', year: '1950', company_city: 'Brisbane', company_country: 'AU', company_line1: 'First Street', company_postal_code: '4000', company_state: 'QLD', personal_postal_code: '4000', personal_city: 'Brisbane', personal_state: 'QLD', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.file = await TestHelper.uploadFile(user)
      req.account = user.account
      req.session = user.session
      req.body = {
        company_city: 'Brisbane',
        company_state: 'QLD',
        company_line1: 'Address First Line',
        company_postal_code: '4000',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Australian',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it('should submit AU information', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'AU', day: '1', month: '1', year: '1950', company_city: 'Brisbane', company_country: 'AU', company_line1: 'First Street', company_postal_code: '4000', company_state: 'QLD', personal_postal_code: '4000', personal_city: 'Brisbane', personal_state: 'QLD', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        company_city: 'Brisbane',
        company_state: 'QLD',
        company_line1: 'Address First Line',
        company_postal_code: '4000',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Australian',
        last_name: 'Person'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.post(req, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should reject BE invalid fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'BE', day: '1', month: '1', year: '1950', company_city: 'Brussels', company_postal_code: '1020', company_line1: 'First street', personal_city: 'Brussels', personal_postal_code: '1020', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.file = await TestHelper.uploadFile(user)
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Brussels',
        personal_line1: 'Address First Line',
        personal_postal_code: '1020',
        company_city: 'Brussels',
        company_line1: 'Address First Line',
        company_postal_code: '1020',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it('should submit BE information', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'BE', day: '1', month: '1', year: '1950', company_city: 'Brussels', company_postal_code: '1020', company_line1: 'First street', personal_city: 'Brussels', personal_postal_code: '1020', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Brussels',
        personal_line1: 'Address First Line',
        personal_postal_code: '1020',
        company_city: 'Brussels',
        company_line1: 'Address First Line',
        company_postal_code: '1020',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.post(req, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should reject CA invalid fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'CA', day: '1', month: '1', year: '1950', company_city: 'Vancouver', company_state: 'BC', company_postal_code: 'V7G 0A1', company_line1: 'First Street', personal_city: 'Vancouver', personal_state: 'BC', personal_postal_code: 'V7G 0A1', personal_line1: 'First Street', personal_id_number: '000000000' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.file = await TestHelper.uploadFile(user)
      req.account = user.account
      req.session = user.session
      req.body = {
        company_city: 'Vancouver',
        company_state: 'BC',
        company_line1: 'Address First Line',
        company_postal_code: 'V5K 0A1',
        business_name: 'Company',
        business_tax_id: '8',
        personal_id_number: '7',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Canadian',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it('should submit CA information', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'CA', day: '1', month: '1', year: '1950', company_city: 'Vancouver', company_state: 'BC', company_postal_code: 'V7G 0A1', company_line1: 'First Street', personal_city: 'Vancouver', personal_state: 'BC', personal_postal_code: 'V7G 0A1', personal_line1: 'First Street', personal_id_number: '000000000' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        company_city: 'Vancouver',
        company_state: 'BC',
        company_line1: 'Address First Line',
        company_postal_code: 'V5K 0A1',
        business_name: 'Company',
        business_tax_id: '8',
        personal_id_number: '7',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Canadian',
        last_name: 'Person'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.post(req, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should reject CH invalid fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'CH', day: '1', month: '1', year: '1950', company_city: 'Bern', company_postal_code: '1020', company_line1: 'First Street', personal_city: 'Bern', personal_postal_code: '1020', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.file = await TestHelper.uploadFile(user)
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Bern',
        personal_line1: 'Address First Line',
        personal_postal_code: '1020',
        company_city: 'Bern',
        company_line1: 'Address First Line',
        company_postal_code: '1020',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it('should submit CH information', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'CH', day: '1', month: '1', year: '1950', company_city: 'Bern', company_postal_code: '1020', company_line1: 'First Street', personal_city: 'Bern', personal_postal_code: '1020', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Bern',
        personal_line1: 'Address First Line',
        personal_postal_code: '1020',
        company_city: 'Bern',
        company_line1: 'Address First Line',
        company_postal_code: '1020',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.post(req, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should reject DE invalid fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'DE', day: '1', month: '1', year: '1950', company_city: 'Berlin', company_line1: 'First Street', company_postal_code: '01067', personal_city: 'Berlin', personal_line1: 'First Street', personal_postal_code: '01067' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.file = await TestHelper.uploadFile(user)
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Berlin',
        personal_line1: 'Address First Line',
        personal_postal_code: '01067',
        company_city: 'Berlin',
        company_line1: 'Address First Line',
        company_postal_code: '01067',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it('should submit DE information', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'DE', day: '1', month: '1', year: '1950', company_city: 'Berlin', company_line1: 'First Street', company_postal_code: '01067', personal_city: 'Berlin', personal_line1: 'First Street', personal_postal_code: '01067' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Berlin',
        personal_line1: 'Address First Line',
        personal_postal_code: '01067',
        company_city: 'Berlin',
        company_line1: 'Address First Line',
        company_postal_code: '01067',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.post(req, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should reject DK invalid fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'DK', day: '1', month: '1', year: '1950', company_city: 'Copenhagen', company_postal_code: '2300', company_line1: 'First Street', personal_city: 'Copenhagen', personal_postal_code: '2300', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.file = await TestHelper.uploadFile(user)
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Copenhagen',
        personal_line1: 'Address First Line',
        personal_postal_code: '1000',
        company_city: 'Copenhagen',
        company_line1: 'Address First Line',
        company_postal_code: '1000',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it('should submit DK information', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'DK', day: '1', month: '1', year: '1950', company_city: 'Copenhagen', company_postal_code: '2300', company_line1: 'First Street', personal_city: 'Copenhagen', personal_postal_code: '2300', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Copenhagen',
        personal_line1: 'Address First Line',
        personal_postal_code: '1000',
        company_city: 'Copenhagen',
        company_line1: 'Address First Line',
        company_postal_code: '1000',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.post(req, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should reject ES invalid fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'ES', day: '1', month: '1', year: '1950', company_city: 'Madrid', company_postal_code: '03179', company_line1: 'First Street', personal_city: 'Madrid', personal_postal_code: '03179', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.file = await TestHelper.uploadFile(user)
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Madrid',
        personal_line1: 'Address First Line',
        personal_postal_code: '03179',
        company_city: 'Madrid',
        company_line1: 'Address First Line',
        company_postal_code: '03179',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it('should submit ES information', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'ES', day: '1', month: '1', year: '1950', company_city: 'Madrid', company_postal_code: '03179', company_line1: 'First Street', personal_city: 'Madrid', personal_postal_code: '03179', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Madrid',
        personal_line1: 'Address First Line',
        personal_postal_code: '03179',
        company_city: 'Madrid',
        company_line1: 'Address First Line',
        company_postal_code: '03179',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.post(req, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should reject FI invalid fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'FI', day: '1', month: '1', year: '1950', company_city: 'Helsinki', company_postal_code: '00990', company_line1: 'First Street', personal_city: 'Helsinki', personal_postal_code: '00990', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.file = await TestHelper.uploadFile(user)
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Helsinki',
        personal_line1: 'Address First Line',
        personal_postal_code: '00990',
        company_city: 'Helsinki',
        company_line1: 'Address First Line',
        company_postal_code: '00990',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it('should submit FI information', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'FI', day: '1', month: '1', year: '1950', company_city: 'Helsinki', company_postal_code: '00990', company_line1: 'First Street', personal_city: 'Helsinki', personal_postal_code: '00990', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Helsinki',
        personal_line1: 'Address First Line',
        personal_postal_code: '00990',
        company_city: 'Helsinki',
        company_line1: 'Address First Line',
        company_postal_code: '00990',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.post(req, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should reject FR invalid fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'FR', day: '1', month: '1', year: '1950', company_city: 'Paris', company_postal_code: '75001', company_line1: 'First Street', personal_city: 'Paris', personal_postal_code: '75001', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.file = await TestHelper.uploadFile(user)
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Paris',
        personal_line1: 'Address First Line',
        personal_postal_code: '75001',
        company_city: 'Paris',
        company_line1: 'Address First Line',
        company_postal_code: '75001',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it('should submit FR information', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'FR', day: '1', month: '1', year: '1950', company_city: 'Paris', company_postal_code: '75001', company_line1: 'First Street', personal_city: 'Paris', personal_postal_code: '75001', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Paris',
        personal_line1: 'Address First Line',
        personal_postal_code: '75001',
        company_city: 'Paris',
        company_line1: 'Address First Line',
        company_postal_code: '75001',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.post(req, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should reject GB invalid fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'GB', day: '1', month: '1', year: '1950', company_city: 'London', company_postal_code: 'EC1A 1AA', company_line1: 'First Street', personal_city: 'London', personal_postal_code: 'EC1A 1AA', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.file = await TestHelper.uploadFile(user)
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'London',
        personal_line1: 'Address First Line',
        personal_postal_code: 'EC1A 1AA',
        company_city: 'London',
        company_line1: 'Address First Line',
        company_postal_code: 'EC1A 1AA',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it('should submit GB information', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'GB', day: '1', month: '1', year: '1950', company_city: 'London', company_postal_code: 'EC1A 1AA', company_line1: 'First Street', personal_city: 'London', personal_postal_code: 'EC1A 1AA', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'London',
        personal_line1: 'Address First Line',
        personal_postal_code: 'EC1A 1AA',
        company_city: 'London',
        company_line1: 'Address First Line',
        company_postal_code: 'EC1A 1AA',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.post(req, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should reject HK invalid fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'HK', personal_id_number: '7', line1: 'First Street', day: '1', month: '1', year: '1950', company_city: 'Hong Kong', personal_city: 'Hong Kong', company_line1: 'First Street', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.file = await TestHelper.uploadFile(user)
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Hong Kong',
        personal_line1: 'Address First Line',
        company_city: 'Hong Kong',
        company_line1: 'Address First Line',
        business_name: 'Company',
        business_tax_id: '8',
        personal_id_number: '7',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Hongkonger',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it('should submit HK information', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'HK', personal_id_number: '7', line1: 'First Street', day: '1', month: '1', year: '1950', company_city: 'Hong Kong', personal_city: 'Hong Kong', company_line1: 'First Street', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Hong Kong',
        personal_line1: 'Address First Line',
        company_city: 'Hong Kong',
        company_line1: 'Address First Line',
        business_name: 'Company',
        business_tax_id: '8',
        personal_id_number: '7',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Hongkonger',
        last_name: 'Person'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.post(req, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should reject IE invalid fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'IE', day: '1', month: '1', year: '1950', company_city: 'Dublin', company_line1: 'First Street', personal_city: 'Dublin', personal_line1: 'First Street', company_state: 'Dublin', personal_state: 'Dublin' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.file = await TestHelper.uploadFile(user)
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Dublin',
        personal_line1: 'Address First Line',
        personal_state: 'Dublin',
        company_city: 'Dublin',
        company_state: 'Dublin',
        company_line1: 'Address First Line',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it('should submit IE information', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'IE', day: '1', month: '1', year: '1950', company_city: 'Dublin', company_line1: 'First Street', personal_city: 'Dublin', personal_line1: 'First Street', company_state: 'Dublin', personal_state: 'Dublin' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Dublin',
        personal_line1: 'Address First Line',
        personal_state: 'Dublin',
        company_city: 'Dublin',
        company_state: 'Dublin',
        company_line1: 'Address First Line',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.post(req, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should reject IT invalid fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'IT', day: '1', month: '1', year: '1950', company_city: 'Rome', company_postal_code: '10001', company_line1: 'First Street', personal_city: 'Rome', personal_line1: 'First Street', personal_postal_code: '00010' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.file = await TestHelper.uploadFile(user)
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Rome',
        personal_line1: 'Address First Line',
        personal_postal_code: '00010',
        company_city: 'Rome',
        company_line1: 'Address First Line',
        company_postal_code: '00010',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it('should submit IT information', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'IT', day: '1', month: '1', year: '1950', company_city: 'Rome', company_postal_code: '10001', company_line1: 'First Street', personal_city: 'Rome', personal_line1: 'First Street', personal_postal_code: '00010' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Rome',
        personal_line1: 'Address First Line',
        personal_postal_code: '00010',
        company_city: 'Rome',
        company_line1: 'Address First Line',
        company_postal_code: '00010',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.post(req, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should reject JP invalid fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', business_name_kana: user.profile.firstName + '\'s company', business_name_kanji: user.profile.firstName + '\'s company', country: 'JP', day: '1', month: '1', year: '1950', gender: 'female', first_name_kana: 'ﾄｳｷﾖｳﾄ', last_name_kana: 'ﾄｳｷﾖｳﾄ', first_name_kanji: '東京都', last_name_kanji: '東京都', phone_number: '0859-076500', personal_postal_code_kana: '1500001', personal_state_kana: 'ﾄｳｷﾖｳﾄ', personal_city_kana: 'ｼﾌﾞﾔ', personal_town_kana: 'ｼﾞﾝｸﾞｳﾏｴ 3-', personal_line1_kana: '27-15', personal_postal_code_kanji: '１５００００１', personal_state_kanji: '東京都', personal_city_kanji: '渋谷区', personal_town_kanji: '神宮前　３丁目', personal_line1_kanji: '２７－１５', company_postal_code_kana: '1500001', company_state_kana: 'ﾄｳｷﾖｳﾄ', company_city_kana: 'ｼﾌﾞﾔ', company_town_kana: 'ｼﾞﾝｸﾞｳﾏｴ 3-', company_line1_kana: '27-15', company_postal_code_kanji: '１５００００１', company_state_kanji: '東京都', company_city_kanji: '渋谷区', company_town_kanji: '神宮前　３丁目', company_line1_kanji: '２７－１５' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.file = await TestHelper.uploadFile(user)
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        gender: 'female',
        first_name_kana: 'ﾄｳｷﾖｳﾄ',
        last_name_kana: 'ﾄｳｷﾖｳﾄ',
        first_name_kanji: '東京都',
        last_name_kanji: '東京都',
        phone_number: '0859-076500',
        business_name: 'Company',
        business_name_kana: 'ﾄｳｷﾖｳﾄ',
        business_name_kanji: '東京都',
        business_tax_id: '8',
        company_postal_code_kana: '1500001',
        company_state_kana: 'ﾄｳｷﾖｳﾄ',
        company_city_kana: 'ｼﾌﾞﾔ',
        company_town_kana: 'ｼﾞﾝｸﾞｳﾏｴ 3-',
        company_line1_kana: '27-15',
        company_postal_code_kanji: '１５００００１',
        company_state_kanji: '東京都',
        company_city_kanji: '渋谷区',
        company_town_kanji: '神宮前　３丁目',
        company_line1_kanji: '２７－１５',
        personal_state_kana: 'ﾄｳｷﾖｳﾄ',
        personal_city_kana: 'ｼﾌﾞﾔ',
        personal_town_kana: 'ｼﾞﾝｸﾞｳﾏｴ 3-',
        personal_line1_kana: '27-15',
        personal_postal_code_kana: '1500001',
        personal_postal_code_kanji: '１５００００１',
        personal_state_kanji: '東京都',
        personal_city_kanji: '渋谷区',
        personal_town_kanji: '神宮前　３丁目',
        personal_line1_kanji: '２７－１５'
      }
      await testEachFieldAsNull(req)
    })

    it('should submit JP information', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', business_name_kana: user.profile.firstName + '\'s company', business_name_kanji: user.profile.firstName + '\'s company', country: 'JP', day: '1', month: '1', year: '1950', gender: 'female', first_name_kana: 'ﾄｳｷﾖｳﾄ', last_name_kana: 'ﾄｳｷﾖｳﾄ', first_name_kanji: '東京都', last_name_kanji: '東京都', phone_number: '0859-076500', personal_postal_code_kana: '1500001', personal_state_kana: 'ﾄｳｷﾖｳﾄ', personal_city_kana: 'ｼﾌﾞﾔ', personal_town_kana: 'ｼﾞﾝｸﾞｳﾏｴ 3-', personal_line1_kana: '27-15', personal_postal_code_kanji: '１５００００１', personal_state_kanji: '東京都', personal_city_kanji: '渋谷区', personal_town_kanji: '神宮前　３丁目', personal_line1_kanji: '２７－１５', company_postal_code_kana: '1500001', company_state_kana: 'ﾄｳｷﾖｳﾄ', company_city_kana: 'ｼﾌﾞﾔ', company_town_kana: 'ｼﾞﾝｸﾞｳﾏｴ 3-', company_line1_kana: '27-15', company_postal_code_kanji: '１５００００１', company_state_kanji: '東京都', company_city_kanji: '渋谷区', company_town_kanji: '神宮前　３丁目', company_line1_kanji: '２７－１５' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        gender: 'female',
        first_name_kana: 'ﾄｳｷﾖｳﾄ',
        last_name_kana: 'ﾄｳｷﾖｳﾄ',
        first_name_kanji: '東京都',
        last_name_kanji: '東京都',
        phone_number: '0859-076500',
        business_name: 'Company',
        business_name_kana: 'ﾄｳｷﾖｳﾄ',
        business_name_kanji: '東京都',
        business_tax_id: '8',
        company_postal_code_kana: '1500001',
        company_state_kana: 'ﾄｳｷﾖｳﾄ',
        company_city_kana: 'ｼﾌﾞﾔ',
        company_town_kana: 'ｼﾞﾝｸﾞｳﾏｴ 3-',
        company_line1_kana: '27-15',
        company_postal_code_kanji: '１５００００１',
        company_state_kanji: '東京都',
        company_city_kanji: '渋谷区',
        company_town_kanji: '神宮前　３丁目',
        company_line1_kanji: '２７－１５',
        personal_state_kana: 'ﾄｳｷﾖｳﾄ',
        personal_city_kana: 'ｼﾌﾞﾔ',
        personal_town_kana: 'ｼﾞﾝｸﾞｳﾏｴ 3-',
        personal_line1_kana: '27-15',
        personal_postal_code_kana: '1500001',
        personal_postal_code_kanji: '１５００００１',
        personal_state_kanji: '東京都',
        personal_city_kanji: '渋谷区',
        personal_town_kanji: '神宮前　３丁目',
        personal_line1_kanji: '２７－１５'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.post(req, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should reject LU invalid fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'LU', day: '1', month: '1', year: '1950', company_city: 'Luxemburg', company_postal_code: '1623', company_line1: 'First Street', personal_city: 'Luxemburg', personal_postal_code: '1623', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.file = await TestHelper.uploadFile(user)
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Luxemburg',
        personal_line1: 'Address First Line',
        personal_postal_code: '1623',
        company_city: 'Luxemburg',
        company_line1: 'Address First Line',
        company_postal_code: '1623',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it('should submit LU information', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'LU', day: '1', month: '1', year: '1950', company_city: 'Luxemburg', company_postal_code: '1623', company_line1: 'First Street', personal_city: 'Luxemburg', personal_postal_code: '1623', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Luxemburg',
        personal_line1: 'Address First Line',
        personal_postal_code: '1623',
        company_city: 'Luxemburg',
        company_line1: 'Address First Line',
        company_postal_code: '1623',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.post(req, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should reject NL invalid fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'NL', day: '1', month: '1', year: '1950', company_city: 'Amsterdam', company_postal_code: '1071 JA', company_line1: 'First Street', personal_city: 'Amsterdam', personal_postal_code: '1071 JA', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.file = await TestHelper.uploadFile(user)
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Amsterdam',
        personal_line1: 'Address First Line',
        personal_postal_code: '1071 JA',
        company_city: 'Amsterdam',
        company_line1: 'Address First Line',
        company_postal_code: '1071 JA',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it('should submit NL information', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'NL', day: '1', month: '1', year: '1950', company_city: 'Amsterdam', company_postal_code: '1071 JA', company_line1: 'First Street', personal_city: 'Amsterdam', personal_postal_code: '1071 JA', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Amsterdam',
        personal_line1: 'Address First Line',
        personal_postal_code: '1071 JA',
        company_city: 'Amsterdam',
        company_line1: 'Address First Line',
        company_postal_code: '1071 JA',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.post(req, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should reject NO invalid fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'NO', day: '1', month: '1', year: '1950', company_city: 'Oslo', company_postal_code: '0001', company_line1: 'First Street', personal_city: 'Oslo', personal_postal_code: '0001', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.file = await TestHelper.uploadFile(user)
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Oslo',
        personal_line1: 'Address First Line',
        personal_postal_code: '0001',
        company_city: 'Oslo',
        company_line1: 'Address First Line',
        company_postal_code: '0001',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it('should submit NO information', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'NO', day: '1', month: '1', year: '1950', company_city: 'Oslo', company_postal_code: '0001', company_line1: 'First Street', personal_city: 'Oslo', personal_postal_code: '0001', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Oslo',
        personal_line1: 'Address First Line',
        personal_postal_code: '0001',
        company_city: 'Oslo',
        company_line1: 'Address First Line',
        company_postal_code: '0001',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.post(req, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should reject NZ invalid fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'NZ', day: '1', month: '1', year: '1950', company_city: 'Auckland', company_postal_code: '6011', company_line1: 'First Street', personal_city: 'Auckland', personal_postal_code: '6011', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.file = await TestHelper.uploadFile(user)
      req.account = user.account
      req.session = user.session
      req.body = {
        company_city: 'Auckland',
        company_line1: 'Address First Line',
        company_postal_code: '6011',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it('should submit NZ information', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'NZ', day: '1', month: '1', year: '1950', company_city: 'Auckland', company_postal_code: '6011', company_line1: 'First Street', personal_city: 'Auckland', personal_postal_code: '6011', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        company_city: 'Auckland',
        company_line1: 'Address First Line',
        company_postal_code: '6011',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.post(req, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should reject PT invalid fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'PT', day: '1', month: '1', year: '1950', company_city: 'Lisbon', company_postal_code: '4520', company_line1: 'First Street', personal_city: 'Lisbon', personal_postal_code: '4520', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.file = await TestHelper.uploadFile(user)
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Lisbon',
        personal_line1: 'Address First Line',
        personal_postal_code: '4520',
        company_city: 'Lisbon',
        company_line1: 'Address First Line',
        company_postal_code: '4520',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it('should submit PT information', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'PT', day: '1', month: '1', year: '1950', company_city: 'Lisbon', company_postal_code: '4520', company_line1: 'First Street', personal_city: 'Lisbon', personal_postal_code: '4520', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Lisbon',
        personal_line1: 'Address First Line',
        personal_postal_code: '4520',
        company_city: 'Lisbon',
        company_line1: 'Address First Line',
        company_postal_code: '4520',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.post(req, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should reject SE invalid fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'SE', day: '1', month: '1', year: '1950', company_city: 'Stockholm', company_postal_code: '00150', company_line1: 'First Street', personal_city: 'Stockholm', personal_postal_code: '00150', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.file = await TestHelper.uploadFile(user)
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Stockholm',
        personal_line1: 'Address First Line',
        personal_postal_code: '00150',
        company_city: 'Stockholm',
        company_line1: 'Address First Line',
        company_postal_code: '00150',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it('should submit SE information', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'SE', day: '1', month: '1', year: '1950', company_city: 'Stockholm', company_postal_code: '00150', company_line1: 'First Street', personal_city: 'Stockholm', personal_postal_code: '00150', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        personal_city: 'Stockholm',
        personal_line1: 'Address First Line',
        personal_postal_code: '00150',
        company_city: 'Stockholm',
        company_line1: 'Address First Line',
        company_postal_code: '00150',
        business_name: 'Company',
        business_tax_id: '8',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.post(req, res2)
      }
      return req.route.api.post(req, res)
    })

    // these tests only work if your Stripe account is SG
    // it('should reject SG invalid fields', async () => {
    //   const user = await TestHelper.createUser()
    //   await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'SG', postal_code: '339696', line1: 'First Street', day: '1', month: '1', year: '1950', company_line1: 'First Street', company_postal_code: '339696', personal_line1: 'First Street', personal_postal_code: '339696', personal_id_number: '000000000' })
    //   const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
    //   req.file = await TestHelper.uploadFile(user)
    //   req.account = user.account
    //   req.session = user.session
    //   req.body = {
    //     personal_line1: 'Address First Line',
    //     personal_postal_code: '339696',
    //     company_line1: 'Address First Line',
    //     company_postal_code: '339696',
    //     business_name: 'Company',
    //     business_tax_id: '8',
    //     personal_id_number: '7',
    //     day: '1',
    //     month: '1',
    //     year: '1950',
    //     first_name: 'Singaporean',
    //     last_name: 'Person'
    //   }
    //   await testEachFieldAsNull(req)
    // })

    // it('should submit SG information', async () => {
    //   const user = await TestHelper.createUser()
    //   await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'SG', postal_code: '339696', line1: 'First Street', day: '1', month: '1', year: '1950', company_line1: 'First Street', company_postal_code: '339696', personal_line1: 'First Street', personal_postal_code: '339696', personal_id_number: '000000000' })
    //   const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
    //   req.account = user.account
    //   req.session = user.session
    //   req.body = {
    //     personal_line1: 'Address First Line',
    //     personal_postal_code: '339696',
    //     company_line1: 'Address First Line',
    //     company_postal_code: '339696',
    //     business_name: 'Company',
    //     business_tax_id: '8',
    //     personal_id_number: '7',
    //     day: '1',
    //     month: '1',
    //     year: '1950',
    //     first_name: 'Singaporean',
    //     last_name: 'Person'
    //   }
    //   const res = TestHelper.createResponse()
    //   res.end = async (str) => {
    //     req.session = await TestHelper.unlockSession(user)
    //     const res2 = TestHelper.createResponse()
    //     res2.end = async (str) => {
    //       const doc = TestHelper.extractDoc(str)
    //       const messageContainer = doc.getElementById('message-container')
    //       const message = messageContainer.child[0]
    //       assert.strictEqual(message.attr.template, 'success')
    //     }
    //     return req.route.api.post(req, res2)
    //   }
    //   return req.route.api.post(req, res)
    // })

    it('should reject US invalid fields', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'US', city: 'New York City', personal_city: 'New York City', postal_code: '10001', personal_id_number: '000000000', line1: 'First Street', day: '1', month: '1', year: '1950', company_city: 'New York City', company_state: 'New York', company_line1: 'First Street', company_postal_code: '10001', ssn_last_4: '0000' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.file = await TestHelper.uploadFile(user)
      req.account = user.account
      req.session = user.session
      req.body = {
        business_name: 'Company',
        business_tax_id: '8',
        company_city: 'New York City',
        company_line1: 'Address First Line',
        company_postal_code: '10001',
        company_state: 'NY',
        personal_id_number: '123451234',
        ssn_last_4: '1234',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'American',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it('should submit US information', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'US', city: 'New York City', personal_city: 'New York City', postal_code: '10001', personal_id_number: '000000000', line1: 'First Street', day: '1', month: '1', year: '1950', company_city: 'New York City', company_state: 'New York', company_line1: 'First Street', company_postal_code: '10001', ssn_last_4: '0000' })
      const req = TestHelper.createRequest(`/account/connect/edit-company-registration?stripeid=${user.stripeAccount.id}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        business_name: 'Company',
        business_tax_id: '8',
        company_city: 'New York City',
        company_line1: 'Address First Line',
        company_postal_code: '10001',
        company_state: 'NY',
        personal_id_number: '123451234',
        ssn_last_4: '1234',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'American',
        last_name: 'Person'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.post(req, res2)
      }
      return req.route.api.post(req, res)
    })
  })
})
