const dashboard = require('@userappstore/dashboard')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.stripeid) {
      throw new Error('invalid-stripeid')
    }
    return dashboard.RedisList.count(`stripeAccount:payouts:${req.query.stripeid}`)
  }
}
