/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../../test-helper.js')

describe('/api/user/connect/country-spec', () => {
  describe('CountrySpec#GET', () => {
    it('should reject invalid country', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/connect/country-spec?country=invalid`, 'GET')
      req.account = user.account
      req.session = user.session
      let errorMessage
      try {
        await req.route.api.get(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-country')
    })

    it('should return stripe countrySpec data', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/connect/country-spec?country=US`, 'GET')
      req.account = user.account
      req.session = user.session
      const countrySpec = await req.route.api.get(req)
      assert.strictEqual(countrySpec.id, 'US')
    })
  })
})
