const connect = require('../../../../../index.js')
const stripe = require('stripe')()
const stripeCache = require('@userappstore/stripe-subscriptions/src/stripe-cache.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.ownerid) {
      throw new Error('invalid-ownerid')
    }
    const owner = await global.api.user.connect.AdditionalOwner.get(req)
    req.query.stripeid = owner.stripeid
    const stripeAccount = await global.api.user.connect.StripeAccount.get(req)
    if (stripeAccount.metadata.submitted || stripeAccount.metadata.submittedOwners) {
      throw new Error('invalid-stripe-account')
    }
    const owners = await global.api.user.connect.AdditionalOwners.get(req)
    for (const i in owners) {
      if (owners[i].ownerid !== req.query.ownerid) {
        continue
      }
      owners.splice(i, 1)
      break
    }
    req.stripeAccount = stripeAccount
    req.owners = owners
  },
  delete: async (req) => {
    const accountInfo = {
      metadata: {
      }
    }
    connect.MetaData.store(accountInfo.metadata, 'owners', req.owners)
    try {
      const accountNow = await stripe.accounts.update(req.stripeAccount.id, accountInfo, req.stripeKey)
      await global.redisClient.hdelAsync(`map:ownerid:stripeid`, req.query.ownerid)
      req.success = true
      await stripeCache.update(accountNow)
      return accountNow
    } catch (error) {
      throw new Error('unknown-error')
    }
  }
}
