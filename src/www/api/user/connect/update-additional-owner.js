const connect = require('../../../../../index.js')
const stripe = require('stripe')()
const stripeCache = require('@userappstore/stripe-subscriptions/src/stripe-cache.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.ownerid) {
      throw new Error('invalid-ownerid')
    }
    if (!req.body || !req.body.city) {
      throw new Error('invalid-city')
    }
    if (!req.body.country) {
      throw new Error('invalid-country')
    }
    if (!req.body.line1) {
      throw new Error('invalid-line1')
    }
    if (!req.body.postal_code) {
      throw new Error('invalid-postal_code')
    }
    if (!req.body.day) {
      throw new Error('invalid-day')
    }
    if (!req.body.month) {
      throw new Error('invalid-month')
    }
    if (!req.body.year) {
      throw new Error('invalid-year')
    }
    if (!req.body.first_name) {
      throw new Error('invalid-first_name')
    }
    if (!req.body.last_name) {
      throw new Error('invalid-last_name')
    }
    const owner = await global.api.user.connect.AdditionalOwner.get(req)
    req.query.stripeid = owner.stripeid
    const stripeAccount = await global.api.user.connect.StripeAccount.get(req)
    if (stripeAccount.metadata.submitted || stripeAccount.metadata.submittedOwners) {
      throw new Error('invalid-stripe-account')
    }
    const owners = connect.MetaData.parse(stripeAccount.metadata, 'owners')
    req.owner = owner
    req.owners = owners
    req.stripeAccount = stripeAccount
  },
  patch: async (req) => {
    for (const field in req.body) {
      req.owner[field] = req.body[field]
    }
    if (req.file) {
      req.owner.documentid = req.file.id
    }
    if (req.owners && req.owners.length) {
      for (const i in req.owners) {
        if (req.owners[i].ownerid === req.query.ownerid) {
          req.owners[i] = req.owner
          break
        }
      }
    } else {
      req.owners = [req.owner]
    }
    const accountInfo = {
      metadata: {
      }
    }
    connect.MetaData.store(accountInfo.metadata, 'owners', req.owners)
    try {
      const accountNow = await stripe.accounts.update(req.stripeAccount.id, accountInfo, req.stripeKey)
      await stripeCache.update(accountNow)
      req.success = true
      return accountNow
    } catch (error) {
      if (error.message.startsWith('invalid-')) {
        throw error
      }
      throw new Error('unknown-error')
    }
  }
}
