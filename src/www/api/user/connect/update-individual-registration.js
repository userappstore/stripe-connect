const connect = require('../../../../../index.js')
const stripe = require('stripe')()
const stripeCache = require('@userappstore/stripe-subscriptions/src/stripe-cache.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.stripeid) {
      throw new Error('invalid-stripeid')
    }
    const stripeAccount = await global.api.user.connect.StripeAccount.get(req)
    if (stripeAccount.metadata.submitted ||
        stripeAccount.legal_entity.type !== 'individual' ||
        stripeAccount.metadata.accountid !== req.account.accountid) {
      throw new Error('invalid-stripe-account')
    }
    req.query.country = stripeAccount.country
    const countrySpec = await global.api.user.connect.CountrySpec.get(req)
    const requiredFields = countrySpec.verification_fields.individual.minimum.concat(countrySpec.verification_fields.individual.additional)
    for (const pathAndField of requiredFields) {
      let field = pathAndField.split('.').pop()
      if (field === 'external_account' ||
        field === 'type' ||
        field === 'ip' ||
        field === 'date' ||
        field === 'document') {
        continue
      }
      if (stripeAccount.country === 'JP') {
        if (pathAndField.startsWith('legal_entity.address_kana.')) {
          field += '_kana'
        } else if (pathAndField.startsWith('legal_entity.address_kanji.')) {
          field += '_kanji'
        }
      }
      if (!req.body[field]) {
        throw new Error(`invalid-${field}`)
      }
    }
    req.countrySpec = countrySpec
    req.stripeAccount = stripeAccount
  },
  patch: async (req) => {
    const requiredFields = req.countrySpec.verification_fields.individual.minimum.concat(req.countrySpec.verification_fields.individual.additional)
    const registration = connect.MetaData.parse(req.stripeAccount.metadata, 'registration') || {}
    for (const pathAndField of requiredFields) {
      let field = pathAndField.split('.').pop()
      if (field === 'external_account' ||
        field === 'type' ||
        field === 'ip' ||
        field === 'date' ||
        field === 'document') {
        continue
      }
      if (req.stripeAccount.country === 'JP') {
        if (pathAndField.startsWith('legal_entity.address_kana.')) {
          field += '_kana'
        } else if (pathAndField.startsWith('legal_entity.address_kanji.')) {
          field += '_kanji'
        }
      }
      for (const field in req.body) {
        registration[field] = req.body[field]
      }
    }
    const accountInfo = {
      metadata: {}
    }
    connect.MetaData.store(accountInfo.metadata, 'registration', registration)
    try {
      const accountNow = await stripe.accounts.update(req.query.stripeid, accountInfo, req.stripeKey)
      req.success = true
      await stripeCache.update(accountNow)
      return accountNow
    } catch (error) {
      if (error.message.startsWith('invalid-')) {
        throw error
      }
      throw new Error('unknown-error')
    }
  }
}
