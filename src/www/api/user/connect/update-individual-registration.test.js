/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../../test-helper.js')

async function testEachFieldAsNull (req) {
  let errors = 0
  for (const field in req.body) {
    const valueWas = req.body[field]
    req.body[field] = null
    try {
      await req.route.api.patch(req)
    } catch (error) {
      assert.strictEqual(error.message, `invalid-${field}`)
      errors++
    }
    req.body[field] = valueWas
  }
  assert.strictEqual(errors, Object.keys(req.body).length)
}

describe('/api/user/connect/update-individual-registration', () => {
  describe('UpdateIndividualRegistration#PATCH', () => {
    it('should reject invalid stripeid', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=invalid`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {}
      let errorMessage
      try {
        await req.route.api.patch(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-stripeid')
    })

    it('should reject company Stripe account', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'US', city: 'New York City', personal_city: 'New York City', postal_code: '10001', personal_id_number: '000000000', line1: 'First Street', day: '1', month: '1', year: '1950', company_city: 'New York City', company_state: 'New York', company_line1: 'First Street', company_postal_code: '10001', ssn_last_4: '0000' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        fileid: 'invalid'
      }
      req.body = {}
      let errorMessage
      try {
        await req.route.api.patch(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-stripe-account')
    })

    it('should reject other account\'s Stripe account', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'US', city: 'New York City', postal_code: '10001', personal_id_number: '000000000', line1: 'First Street', day: '1', month: '1', year: '1950', state: 'New York', ssn_last_4: '0000' })
      const user2 = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user2.account
      req.session = user2.session
      req.body = {}
      let errorMessage
      try {
        await req.route.api.patch(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-account')
    })

    it('should reject submitted Stripe account', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'US', city: 'New York City', postal_code: '10001', personal_id_number: '000000000', line1: 'First Street', day: '1', month: '1', year: '1950', state: 'New York', ssn_last_4: '0000' })
      await TestHelper.createExternalAccount(user, { currency: 'usd', country: 'US', account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`, account_type: 'individual', account_number: '000123456789', routing_number: '110000000' })
      await TestHelper.createDocumentUpload(user)
      await TestHelper.submitStripeAccount(user)
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        fileid: 'invalid'
      }
      req.body = {}
      let errorMessage
      try {
        await req.route.api.patch(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-stripe-account')
    })

    it(`should reject AT-individual invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'AT', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update AT-individual registration`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'AT', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      const registrationNow = JSON.parse(accountNow.metadata.registration)
      for (const field in req.body) {
        assert.strictEqual(registrationNow[field], req.body[field])
      }
    })

    it(`should reject AU-individual invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'AU', city: 'Brisbane', postal_code: '4000', line1: 'First Street', day: '1', month: '1', year: '1950', state: 'QLD' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        city: 'Brisbane',
        line1: 'Address First Line',
        postal_code: '4000',
        state: 'QLD',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Australian',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update AU-individual registration`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'AU', city: 'Brisbane', postal_code: '4000', line1: 'First Street', day: '1', month: '1', year: '1950', state: 'QLD' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        city: 'Brisbane',
        state: 'QLD',
        line1: 'Address First Line',
        postal_code: '4000',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Australian',
        last_name: 'Person'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      const registrationNow = JSON.parse(accountNow.metadata.registration)
      for (const field in req.body) {
        assert.strictEqual(registrationNow[field], req.body[field])
      }
    })

    it(`should reject BE-individual invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'BE', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update BE-individual registration`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'BE', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      const registrationNow = JSON.parse(accountNow.metadata.registration)
      for (const field in req.body) {
        assert.strictEqual(registrationNow[field], req.body[field])
      }
    })

    it(`should reject CA-individual invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'CA', city: 'Vancouver', state: 'BC', postal_code: 'V7G 0A1', line1: 'First Street', day: '1', month: '1', year: '1950', personal_id_number: '000000000' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        city: 'Vancouver',
        line1: 'Address First Line',
        postal_code: 'V5K 0A1',
        state: 'BC',
        personal_id_number: '7',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Canadian',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update CA-individual registration`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'CA', city: 'Vancouver', state: 'BC', postal_code: 'V7G 0A1', line1: 'First Street', day: '1', month: '1', year: '1950', personal_id_number: '000000000' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        city: 'Vancouver',
        line1: 'Address First Line',
        postal_code: 'V5K 0A1',
        state: 'BC',
        personal_id_number: '7',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Canadian',
        last_name: 'Person'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      const registrationNow = JSON.parse(accountNow.metadata.registration)
      for (const field in req.body) {
        assert.strictEqual(registrationNow[field], req.body[field])
      }
    })

    it(`should reject CH-individual invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'CH', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update CH-individual registration`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'CH', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      const registrationNow = JSON.parse(accountNow.metadata.registration)
      for (const field in req.body) {
        assert.strictEqual(registrationNow[field], req.body[field])
      }
    })

    it(`should reject DE-individual invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'DE', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update DE-individual registration`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'DE', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      const registrationNow = JSON.parse(accountNow.metadata.registration)
      for (const field in req.body) {
        assert.strictEqual(registrationNow[field], req.body[field])
      }
    })

    it(`should reject DK-individual invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'DK', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update DK-individual registration`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'DK', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      const registrationNow = JSON.parse(accountNow.metadata.registration)
      for (const field in req.body) {
        assert.strictEqual(registrationNow[field], req.body[field])
      }
    })

    it(`should reject ES-individual invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'ES', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update ES-individual registration`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'ES', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      const registrationNow = JSON.parse(accountNow.metadata.registration)
      for (const field in req.body) {
        assert.strictEqual(registrationNow[field], req.body[field])
      }
    })

    it(`should reject FI-individual invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'FI', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update FI-individual registration`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'FI', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      const registrationNow = JSON.parse(accountNow.metadata.registration)
      for (const field in req.body) {
        assert.strictEqual(registrationNow[field], req.body[field])
      }
    })

    it(`should reject FR-individual invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'FR', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update FR-individual registration`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'FR', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      const registrationNow = JSON.parse(accountNow.metadata.registration)
      for (const field in req.body) {
        assert.strictEqual(registrationNow[field], req.body[field])
      }
    })

    it(`should reject GB-individual invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'GB', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update GB-individual registration`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'GB', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      const registrationNow = JSON.parse(accountNow.metadata.registration)
      for (const field in req.body) {
        assert.strictEqual(registrationNow[field], req.body[field])
      }
    })

    it(`should reject HK-individual invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'HK', city: 'Hong Kong', personal_id_number: '7', line1: 'First Street', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        city: 'Hong Kong',
        line1: 'Address First Line',
        personal_id_number: '7',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Hongkonger',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update HK-individual registration`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'HK', city: 'Hong Kong', personal_id_number: '7', line1: 'First Street', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        city: 'Hong Kong',
        line1: 'Address First Line',
        personal_id_number: '7',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Hongkonger',
        last_name: 'Person'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      const registrationNow = JSON.parse(accountNow.metadata.registration)
      for (const field in req.body) {
        assert.strictEqual(registrationNow[field], req.body[field])
      }
    })

    it(`should reject IE-individual invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'IE', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update IE-individual registration`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'IE', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      const registrationNow = JSON.parse(accountNow.metadata.registration)
      for (const field in req.body) {
        assert.strictEqual(registrationNow[field], req.body[field])
      }
    })

    it(`should reject IT-individual invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'IT', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update IT-individual registration`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'IT', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      const registrationNow = JSON.parse(accountNow.metadata.registration)
      for (const field in req.body) {
        assert.strictEqual(registrationNow[field], req.body[field])
      }
    })

    it(`should reject JP-individual invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'JP', day: '1', month: '1', year: '1950', gender: 'female', first_name_kana: 'ﾄｳｷﾖｳﾄ', last_name_kana: 'ﾄｳｷﾖｳﾄ', first_name_kanji: '東京都', last_name_kanji: '東京都', phone_number: '0859-076500', postal_code_kana: '1500001', state_kana: 'ﾄｳｷﾖｳﾄ', city_kana: 'ｼﾌﾞﾔ', town_kana: 'ｼﾞﾝｸﾞｳﾏｴ 3-', line1_kana: '27-15', postal_code_kanji: '１５００００１', state_kanji: '東京都', city_kanji: '渋谷区', town_kanji: '神宮前　３丁目', line1_kanji: '２７－１５' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        gender: 'female',
        first_name_kana: 'ﾄｳｷﾖｳﾄ',
        last_name_kana: 'ﾄｳｷﾖｳﾄ',
        first_name_kanji: '東京都',
        last_name_kanji: '東京都',
        phone_number: '0859-076500',
        postal_code_kana: '1500001',
        state_kana: 'ﾄｳｷﾖｳﾄ',
        city_kana: 'ｼﾌﾞﾔ',
        town_kana: 'ｼﾞﾝｸﾞｳﾏｴ 3-',
        line1_kana: '27-15',
        postal_code_kanji: '１５００００１',
        state_kanji: '東京都',
        city_kanji: '渋谷区',
        town_kanji: '神宮前　３丁目',
        line1_kanji: '２７－１５'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update JP-individual registration`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'JP', day: '1', month: '1', year: '1950', gender: 'female', first_name_kana: 'ﾄｳｷﾖｳﾄ', last_name_kana: 'ﾄｳｷﾖｳﾄ', first_name_kanji: '東京都', last_name_kanji: '東京都', phone_number: '0859-076500', postal_code_kana: '1500001', state_kana: 'ﾄｳｷﾖｳﾄ', city_kana: 'ｼﾌﾞﾔ', town_kana: 'ｼﾞﾝｸﾞｳﾏｴ 3-', line1_kana: '27-15', postal_code_kanji: '１５００００１', state_kanji: '東京都', city_kanji: '渋谷区', town_kanji: '神宮前　３丁目', line1_kanji: '２７－１５' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        gender: 'female',
        first_name_kana: 'ﾄｳｷﾖｳﾄ',
        last_name_kana: 'ﾄｳｷﾖｳﾄ',
        first_name_kanji: '東京都',
        last_name_kanji: '東京都',
        phone_number: '0859-076500',
        postal_code_kana: '1500001',
        state_kana: 'ﾄｳｷﾖｳﾄ',
        city_kana: 'ｼﾌﾞﾔ',
        town_kana: 'ｼﾞﾝｸﾞｳﾏｴ 3-',
        line1_kana: '27-15',
        postal_code_kanji: '１５００００１',
        state_kanji: '東京都',
        city_kanji: '渋谷区',
        town_kanji: '神宮前　３丁目',
        line1_kanji: '２７－１５'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      const registrationNow = JSON.parse(accountNow.metadata.registration)
      for (const field in req.body) {
        assert.strictEqual(registrationNow[field], req.body[field])
      }
    })

    it(`should reject LU-individual invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'LU', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update LU-individual registration`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'LU', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      const registrationNow = JSON.parse(accountNow.metadata.registration)
      for (const field in req.body) {
        assert.strictEqual(registrationNow[field], req.body[field])
      }
    })

    it(`should reject NL-individual invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'NL', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update NL-individual registration`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'NL', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      const registrationNow = JSON.parse(accountNow.metadata.registration)
      for (const field in req.body) {
        assert.strictEqual(registrationNow[field], req.body[field])
      }
    })

    it(`should reject NO-individual invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'NO', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update NO-individual registration`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'NO', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      const registrationNow = JSON.parse(accountNow.metadata.registration)
      for (const field in req.body) {
        assert.strictEqual(registrationNow[field], req.body[field])
      }
    })

    it(`should reject NZ-individual invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'NZ', city: 'Auckland', postal_code: '6011', line1: 'First Street', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        city: 'Auckland',
        line1: 'Address First Line',
        postal_code: '6011',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update NZ-individual registration`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'NZ', city: 'Auckland', postal_code: '6011', line1: 'First Street', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        city: 'Auckland',
        line1: 'Address First Line',
        postal_code: '6011',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      const registrationNow = JSON.parse(accountNow.metadata.registration)
      for (const field in req.body) {
        assert.strictEqual(registrationNow[field], req.body[field])
      }
    })

    it(`should reject PT-individual invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'PT', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update PT-individual registration`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'PT', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      const registrationNow = JSON.parse(accountNow.metadata.registration)
      for (const field in req.body) {
        assert.strictEqual(registrationNow[field], req.body[field])
      }
    })

    it(`should reject SE-individual invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'SE', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update SE-individual registration`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'SE', day: '1', month: '1', year: '1950' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'Person',
        last_name: 'Person'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      const registrationNow = JSON.parse(accountNow.metadata.registration)
      for (const field in req.body) {
        assert.strictEqual(registrationNow[field], req.body[field])
      }
    })

    // these tests only work if your Stripe account is SG
    // it(`should reject SG-individual invalid fields`, async () => {
    //   const user = await TestHelper.createUser()
    //   await TestHelper.createStripeAccount(user, { type: 'individual', country: 'SG', postal_code: '339696', line1: 'First Street', day: '1', month: '1', year: '1950', personal_id_number: '000000000' })
    //   const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
    //   req.account = user.account
    //   req.session = user.session
    //   req.body = {
    //     line1: 'Address First Line',
    //     postal_code: '339696',
    //     personal_id_number: '7',
    //     day: '1',
    //     month: '1',
    //     year: '1950',
    //     first_name: 'Singaporean',
    //     last_name: 'Person'
    //   }
    //   await testEachFieldAsNull(req)
    // })

    // it(`should update SG-individual registration`, async () => {
    //   const user = await TestHelper.createUser()
    //   await TestHelper.createStripeAccount(user, { type: 'individual', country: 'SG', postal_code: '339696', line1: 'First Street', day: '1', month: '1', year: '1950', personal_id_number: '000000000' })
    //   const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
    //   req.account = user.account
    //   req.session = user.session
    //   req.body = {
    //     line1: 'Address First Line',
    //     postal_code: '339696',
    //     personal_id_number: '7',
    //     day: '1',
    //     month: '1',
    //     year: '1950',
    //     first_name: 'Singaporean',
    //     last_name: 'Person'
    //   }
    //   await req.route.api.patch(req)
    //   req.session = await TestHelper.unlockSession(user)
    //   const accountNow = await req.route.api.patch(req)
    //   const registrationNow = JSON.parse(accountNow.metadata.registration)
    //   for (const field in req.body) {
    //     assert.strictEqual(registrationNow[field], req.body[field])
    //   }
    // })

    it(`should reject US-individual invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'US', city: 'New York City', postal_code: '10001', personal_id_number: '000000000', line1: 'First Street', day: '1', month: '1', year: '1950', state: 'New York', ssn_last_4: '0000' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        city: 'New York City',
        line1: 'Address First Line',
        postal_code: '10001',
        state: 'NY',
        ssn_last_4: '1234',
        personal_id_number: '000000000',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'American',
        last_name: 'Person'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update US-individual registration`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'individual', country: 'US', city: 'New York City', postal_code: '10001', personal_id_number: '000000000', line1: 'First Street', day: '1', month: '1', year: '1950', state: 'New York', ssn_last_4: '0000' })
      const req = TestHelper.createRequest(`/api/user/connect/update-individual-registration?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        city: 'New York City',
        line1: 'Address First Line',
        postal_code: '10001',
        state: 'NY',
        ssn_last_4: '1234',
        personal_id_number: '000000000',
        day: '1',
        month: '1',
        year: '1950',
        first_name: 'American',
        last_name: 'Person'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      const registrationNow = JSON.parse(accountNow.metadata.registration)
      for (const field in req.body) {
        assert.strictEqual(registrationNow[field], req.body[field])
      }
    })
  })
})
