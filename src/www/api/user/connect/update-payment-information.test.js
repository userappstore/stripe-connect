/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../../test-helper.js')

async function testEachFieldAsNull (req) {
  let errors = 0
  for (const field in req.body) {
    const valueWas = req.body[field]
    req.body[field] = null
    try {
      await req.route.api.patch(req)
    } catch (error) {
      assert.strictEqual(error.message, `invalid-${field}`)
      errors++
    }
    req.body[field] = valueWas
  }
  assert.strictEqual(errors, Object.keys(req.body).length)
}

describe('/api/user/connect/update-payment-information', () => {
  describe('UpdatePaymentInformation#PATCH', () => {
    it('should reject invalid stripeid', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=invalid`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'gbp',
        country: 'GB',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        account_number: '00012345',
        sort_code: '108800'
      }
      let errorMessage
      try {
        await req.route.api.patch(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-stripeid')
    })

    it('should reject other account\'s Stripe account', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'US', city: 'New York City', personal_city: 'New York City', postal_code: '10001', personal_id_number: '000000000', line1: 'First Street', day: '1', month: '1', year: '1950', company_city: 'New York City', company_state: 'New York', company_line1: 'First Street', company_postal_code: '10001', ssn_last_4: '0000' })
      const user2 = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user2.account
      req.session = user2.session
      req.body = {
        currency: 'gbp',
        country: 'GB',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        account_number: '00012345',
        sort_code: '108800'
      }
      let errorMessage
      try {
        await req.route.api.patch(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-account')
    })

    it(`should reject AT invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'AT', day: '1', month: '1', year: '1950', company_city: 'Vienna', company_postal_code: '1020', company_line1: 'First Street', personal_city: 'Vienna', personal_line1: 'First Street', personal_postal_code: '1020' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'AT',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'company',
        iban: 'AT89370400440532013000'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update AT information`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'AT', day: '1', month: '1', year: '1950', company_city: 'Vienna', company_postal_code: '1020', company_line1: 'First Street', personal_city: 'Vienna', personal_line1: 'First Street', personal_postal_code: '1020' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'AT',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'company',
        iban: 'AT89370400440532013000'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      assert.strictEqual(accountNow.external_accounts.data.length, 1)
    })

    it(`should reject AU invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'AU', day: '1', month: '1', year: '1950', company_city: 'Brisbane', company_country: 'AU', company_line1: 'First Street', company_postal_code: '4000', company_state: 'QLD', personal_postal_code: '4000', personal_city: 'Brisbane', personal_state: 'QLD', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'aud',
        country: 'AU',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        account_number: '000123456',
        bsb_number: '110000'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update AU information`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'AU', day: '1', month: '1', year: '1950', company_city: 'Brisbane', company_country: 'AU', company_line1: 'First Street', company_postal_code: '4000', company_state: 'QLD', personal_postal_code: '4000', personal_city: 'Brisbane', personal_state: 'QLD', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'aud',
        country: 'AU',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        account_number: '000123456',
        bsb_number: '110000'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      assert.strictEqual(accountNow.external_accounts.data.length, 1)
    })

    it(`should reject BE invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'BE', day: '1', month: '1', year: '1950', company_city: 'Brussels', company_postal_code: '1020', company_line1: 'First street', personal_city: 'Brussels', personal_postal_code: '1020', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'BE',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'company',
        iban: 'BE89370400440532013000'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update BE information`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'BE', day: '1', month: '1', year: '1950', company_city: 'Brussels', company_postal_code: '1020', company_line1: 'First street', personal_city: 'Brussels', personal_postal_code: '1020', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'BE',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'company',
        iban: 'BE89370400440532013000'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      assert.strictEqual(accountNow.external_accounts.data.length, 1)
    })

    it(`should reject CA invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'CA', day: '1', month: '1', year: '1950', company_city: 'Vancouver', company_state: 'BC', company_postal_code: 'V7G 0A1', company_line1: 'First Street', personal_city: 'Vancouver', personal_state: 'BC', personal_postal_code: 'V7G 0A1', personal_line1: 'First Street', personal_id_number: '000000000' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'cad',
        country: 'CA',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        account_number: '000123456789',
        institution_number: '000',
        transit_number: '11000'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update CA information`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'CA', day: '1', month: '1', year: '1950', company_city: 'Vancouver', company_state: 'BC', company_postal_code: 'V7G 0A1', company_line1: 'First Street', personal_city: 'Vancouver', personal_state: 'BC', personal_postal_code: 'V7G 0A1', personal_line1: 'First Street', personal_id_number: '000000000' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'cad',
        country: 'CA',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        account_number: '000123456789',
        institution_number: '000',
        transit_number: '11000'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      assert.strictEqual(accountNow.external_accounts.data.length, 1)
    })

    it(`should reject CH invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'CH', day: '1', month: '1', year: '1950', company_city: 'Bern', company_postal_code: '1020', company_line1: 'First Street', personal_city: 'Bern', personal_postal_code: '1020', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'CH',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        iban: 'CH89370400440532013000'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update CH information`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'CH', day: '1', month: '1', year: '1950', company_city: 'Bern', company_postal_code: '1020', company_line1: 'First Street', personal_city: 'Bern', personal_postal_code: '1020', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'CH',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'company',
        iban: 'CH89370400440532013000'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      assert.strictEqual(accountNow.external_accounts.data.length, 1)
    })

    it(`should reject DE invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'DE', day: '1', month: '1', year: '1950', company_city: 'Berlin', company_line1: 'First Street', company_postal_code: '01067', personal_city: 'Berlin', personal_line1: 'First Street', personal_postal_code: '01067' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'DE',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        iban: 'DE89370400440532013000'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update DE information`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'DE', day: '1', month: '1', year: '1950', company_city: 'Berlin', company_line1: 'First Street', company_postal_code: '01067', personal_city: 'Berlin', personal_line1: 'First Street', personal_postal_code: '01067' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'DE',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'company',
        iban: 'DE89370400440532013000'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      assert.strictEqual(accountNow.external_accounts.data.length, 1)
    })

    it(`should reject DK invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'DK', day: '1', month: '1', year: '1950', company_city: 'Copenhagen', company_postal_code: '2300', company_line1: 'First Street', personal_city: 'Copenhagen', personal_postal_code: '2300', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'DK',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        iban: 'DK89370400440532013000'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update DK information`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'DK', day: '1', month: '1', year: '1950', company_city: 'Copenhagen', company_postal_code: '2300', company_line1: 'First Street', personal_city: 'Copenhagen', personal_postal_code: '2300', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'DK',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'company',
        iban: 'DK89370400440532013000'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      assert.strictEqual(accountNow.external_accounts.data.length, 1)
    })

    it(`should reject ES invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'ES', day: '1', month: '1', year: '1950', company_city: 'Madrid', company_postal_code: '03179', company_line1: 'First Street', personal_city: 'Madrid', personal_postal_code: '03179', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'ES',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        iban: 'ES89370400440532013000'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update ES information`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'ES', day: '1', month: '1', year: '1950', company_city: 'Madrid', company_postal_code: '03179', company_line1: 'First Street', personal_city: 'Madrid', personal_postal_code: '03179', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'ES',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'company',
        iban: 'ES89370400440532013000'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      assert.strictEqual(accountNow.external_accounts.data.length, 1)
    })

    it(`should reject FI invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'FI', day: '1', month: '1', year: '1950', company_city: 'Helsinki', company_postal_code: '00990', company_line1: 'First Street', personal_city: 'Helsinki', personal_postal_code: '00990', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'FI',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        iban: 'FI89370400440532013000'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update FI information`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'FI', day: '1', month: '1', year: '1950', company_city: 'Helsinki', company_postal_code: '00990', company_line1: 'First Street', personal_city: 'Helsinki', personal_postal_code: '00990', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'FI',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'company',
        iban: 'FI89370400440532013000'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      assert.strictEqual(accountNow.external_accounts.data.length, 1)
    })

    it(`should reject FR invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'FR', day: '1', month: '1', year: '1950', company_city: 'Paris', company_postal_code: '75001', company_line1: 'First Street', personal_city: 'Paris', personal_postal_code: '75001', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'FR',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        iban: 'FR89370400440532013000'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update FR information`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'FR', day: '1', month: '1', year: '1950', company_city: 'Paris', company_postal_code: '75001', company_line1: 'First Street', personal_city: 'Paris', personal_postal_code: '75001', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'FR',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'company',
        iban: 'FR89370400440532013000'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      assert.strictEqual(accountNow.external_accounts.data.length, 1)
    })

    it(`should reject GB invalid fields (account number, sort code)`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'GB', day: '1', month: '1', year: '1950', company_city: 'London', company_postal_code: 'EC1A 1AA', company_line1: 'First Street', personal_city: 'London', personal_postal_code: 'EC1A 1AA', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'gbp',
        country: 'GB',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        account_number: '00012345',
        sort_code: '108800'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update GB information (account number, sort code)`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'GB', day: '1', month: '1', year: '1950', company_city: 'London', company_postal_code: 'EC1A 1AA', company_line1: 'First Street', personal_city: 'London', personal_postal_code: 'EC1A 1AA', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'gbp',
        country: 'GB',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        account_number: '00012345',
        sort_code: '108800'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      assert.strictEqual(accountNow.external_accounts.data.length, 1)
    })

    it(`should reject GB invalid fields (iban)`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'GB', day: '1', month: '1', year: '1950', company_city: 'London', company_postal_code: 'EC1A 1AA', company_line1: 'First Street', personal_city: 'London', personal_postal_code: 'EC1A 1AA', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'GB',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        iban: 'GB89370400440532013000'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update GB information (iban)`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'GB', day: '1', month: '1', year: '1950', company_city: 'London', company_postal_code: 'EC1A 1AA', company_line1: 'First Street', personal_city: 'London', personal_postal_code: 'EC1A 1AA', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'GB',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        iban: 'GB89370400440532013000'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      assert.strictEqual(accountNow.external_accounts.data.length, 1)
    })

    it(`should reject HK invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'HK', personal_id_number: '7', line1: 'First Street', day: '1', month: '1', year: '1950', company_city: 'Hong Kong', personal_city: 'Hong Kong', company_line1: 'First Street', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'hkd',
        country: 'HK',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        account_number: '000123-456',
        clearing_code: '110',
        branch_code: '000'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update HK information`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'HK', personal_id_number: '7', line1: 'First Street', day: '1', month: '1', year: '1950', company_city: 'Hong Kong', personal_city: 'Hong Kong', company_line1: 'First Street', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'hkd',
        country: 'HK',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        account_number: '000123-456',
        clearing_code: '110',
        branch_code: '000'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      assert.strictEqual(accountNow.external_accounts.data.length, 1)
    })

    it(`should reject IE invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'IE', day: '1', month: '1', year: '1950', company_city: 'Dublin', company_line1: 'First Street', personal_city: 'Dublin', personal_line1: 'First Street', company_state: 'Dublin', personal_state: 'Dublin' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'IE',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        iban: 'IE89370400440532013000'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update IE information`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'IE', day: '1', month: '1', year: '1950', company_city: 'Dublin', company_line1: 'First Street', personal_city: 'Dublin', personal_line1: 'First Street', company_state: 'Dublin', personal_state: 'Dublin' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'IE',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'company',
        iban: 'IE89370400440532013000'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      assert.strictEqual(accountNow.external_accounts.data.length, 1)
    })

    it(`should reject IT invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'IT', day: '1', month: '1', year: '1950', company_city: 'Rome', company_postal_code: '10001', company_line1: 'First Street', personal_city: 'Rome', personal_line1: 'First Street', personal_postal_code: '00010' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'IE',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        iban: 'IT89370400440532013000'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update IT information`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'IT', day: '1', month: '1', year: '1950', company_city: 'Rome', company_postal_code: '10001', company_line1: 'First Street', personal_city: 'Rome', personal_line1: 'First Street', personal_postal_code: '00010' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'IT',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'company',
        iban: 'IT89370400440532013000'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      assert.strictEqual(accountNow.external_accounts.data.length, 1)
    })

    it(`should reject JP invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', business_name_kana: user.profile.firstName + '\'s company', business_name_kanji: user.profile.firstName + '\'s company', country: 'JP', day: '1', month: '1', year: '1950', gender: 'female', first_name_kana: 'ﾄｳｷﾖｳﾄ', last_name_kana: 'ﾄｳｷﾖｳﾄ', first_name_kanji: '東京都', last_name_kanji: '東京都', phone_number: '0859-076500', personal_postal_code_kana: '1500001', personal_state_kana: 'ﾄｳｷﾖｳﾄ', personal_city_kana: 'ｼﾌﾞﾔ', personal_town_kana: 'ｼﾞﾝｸﾞｳﾏｴ 3-', personal_line1_kana: '27-15', personal_postal_code_kanji: '１５００００１', personal_state_kanji: '東京都', personal_city_kanji: '渋谷区', personal_town_kanji: '神宮前　３丁目', personal_line1_kanji: '２７－１５', company_postal_code_kana: '1500001', company_state_kana: 'ﾄｳｷﾖｳﾄ', company_city_kana: 'ｼﾌﾞﾔ', company_town_kana: 'ｼﾞﾝｸﾞｳﾏｴ 3-', company_line1_kana: '27-15', company_postal_code_kanji: '１５００００１', company_state_kanji: '東京都', company_city_kanji: '渋谷区', company_town_kanji: '神宮前　３丁目', company_line1_kanji: '２７－１５' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'jpy',
        country: 'JP',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        account_number: '00012345',
        bank_code: '1100',
        branch_code: '000'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update JP information`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', business_name_kana: user.profile.firstName + '\'s company', business_name_kanji: user.profile.firstName + '\'s company', country: 'JP', day: '1', month: '1', year: '1950', gender: 'female', first_name_kana: 'ﾄｳｷﾖｳﾄ', last_name_kana: 'ﾄｳｷﾖｳﾄ', first_name_kanji: '東京都', last_name_kanji: '東京都', phone_number: '0859-076500', personal_postal_code_kana: '1500001', personal_state_kana: 'ﾄｳｷﾖｳﾄ', personal_city_kana: 'ｼﾌﾞﾔ', personal_town_kana: 'ｼﾞﾝｸﾞｳﾏｴ 3-', personal_line1_kana: '27-15', personal_postal_code_kanji: '１５００００１', personal_state_kanji: '東京都', personal_city_kanji: '渋谷区', personal_town_kanji: '神宮前　３丁目', personal_line1_kanji: '２７－１５', company_postal_code_kana: '1500001', company_state_kana: 'ﾄｳｷﾖｳﾄ', company_city_kana: 'ｼﾌﾞﾔ', company_town_kana: 'ｼﾞﾝｸﾞｳﾏｴ 3-', company_line1_kana: '27-15', company_postal_code_kanji: '１５００００１', company_state_kanji: '東京都', company_city_kanji: '渋谷区', company_town_kanji: '神宮前　３丁目', company_line1_kanji: '２７－１５' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'jpy',
        country: 'JP',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        account_number: '00012345',
        bank_code: '1100',
        branch_code: '000'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      assert.strictEqual(accountNow.external_accounts.data.length, 1)
    })

    it(`should reject LU invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'LU', day: '1', month: '1', year: '1950', company_city: 'Luxemburg', company_postal_code: '1623', company_line1: 'First Street', personal_city: 'Luxemburg', personal_postal_code: '1623', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'LU',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        iban: 'LU89370400440532013000'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update LU information`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'LU', day: '1', month: '1', year: '1950', company_city: 'Luxemburg', company_postal_code: '1623', company_line1: 'First Street', personal_city: 'Luxemburg', personal_postal_code: '1623', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'LU',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'company',
        iban: 'LU89370400440532013000'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      assert.strictEqual(accountNow.external_accounts.data.length, 1)
    })

    it(`should reject NL invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'NL', day: '1', month: '1', year: '1950', company_city: 'Amsterdam', company_postal_code: '1071 JA', company_line1: 'First Street', personal_city: 'Amsterdam', personal_postal_code: '1071 JA', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'NL',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        iban: 'NL89370400440532013000'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update NL information`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'NL', day: '1', month: '1', year: '1950', company_city: 'Amsterdam', company_postal_code: '1071 JA', company_line1: 'First Street', personal_city: 'Amsterdam', personal_postal_code: '1071 JA', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'NL',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'company',
        iban: 'NL89370400440532013000'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      assert.strictEqual(accountNow.external_accounts.data.length, 1)
    })

    it(`should reject NO invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'NO', day: '1', month: '1', year: '1950', company_city: 'Oslo', company_postal_code: '0001', company_line1: 'First Street', personal_city: 'Oslo', personal_postal_code: '0001', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'NO',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        iban: 'NO89370400440532013000'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update NO information`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'NO', day: '1', month: '1', year: '1950', company_city: 'Oslo', company_postal_code: '0001', company_line1: 'First Street', personal_city: 'Oslo', personal_postal_code: '0001', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'NO',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'company',
        iban: 'NO89370400440532013000'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      assert.strictEqual(accountNow.external_accounts.data.length, 1)
    })

    it(`should reject NZ invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'NZ', day: '1', month: '1', year: '1950', company_city: 'Auckland', company_postal_code: '6011', company_line1: 'First Street', personal_city: 'Auckland', personal_postal_code: '6011', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'nzd',
        country: 'NZ',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        account_number: '000123456',
        routing_number: '1234'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update NZ information`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'NZ', day: '1', month: '1', year: '1950', company_city: 'Auckland', company_postal_code: '6011', company_line1: 'First Street', personal_city: 'Auckland', personal_postal_code: '6011', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'nzd',
        country: 'NZ',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        account_number: '0000000010',
        routing_number: '110000'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      assert.strictEqual(accountNow.external_accounts.data.length, 1)
    })

    it(`should reject PT invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'PT', day: '1', month: '1', year: '1950', company_city: 'Lisbon', company_postal_code: '4520', company_line1: 'First Street', personal_city: 'Lisbon', personal_postal_code: '4520', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'PT',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        iban: 'PT89370400440532013000'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update PT information`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'PT', day: '1', month: '1', year: '1950', company_city: 'Lisbon', company_postal_code: '4520', company_line1: 'First Street', personal_city: 'Lisbon', personal_postal_code: '4520', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'PT',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'company',
        iban: 'PT89370400440532013000'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      assert.strictEqual(accountNow.external_accounts.data.length, 1)
    })

    it(`should reject SE invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'SE', day: '1', month: '1', year: '1950', company_city: 'Stockholm', company_postal_code: '00150', company_line1: 'First Street', personal_city: 'Stockholm', personal_postal_code: '00150', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'IE',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        iban: 'SE89370400440532013000'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update SE information`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'SE', day: '1', month: '1', year: '1950', company_city: 'Stockholm', company_postal_code: '00150', company_line1: 'First Street', personal_city: 'Stockholm', personal_postal_code: '00150', personal_line1: 'First Street' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'eur',
        country: 'SE',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'company',
        iban: 'SE89370400440532013000'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      assert.strictEqual(accountNow.external_accounts.data.length, 1)
    })

    // these tests only work if your Stripe account is SG
    // it(`should reject SG invalid fields`, async () => {
    //   const user = await TestHelper.createUser()
    //   await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'SG', postal_code: '339696', line1: 'First Street', day: '1', month: '1', year: '1950', company_line1: 'First Street', company_postal_code: '339696', personal_line1: 'First Street', personal_postal_code: '339696', personal_id_number: '000000000' })
    //   const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
    //   req.account = user.account
    //   req.session = user.session
    //   req.body = {
    //     currency: 'sgd',
    //     country: 'SG',
    //     account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
    //     account_type: 'individual',
    //     account_number: '000123456',
    //     bank_code: '1100',
    //     branch_code: '000'
    //   }
    //   await testEachFieldAsNull(req)
    // })

    // it(`should update SG information`, async () => {
    //   const user = await TestHelper.createUser()
    //   await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'SG', postal_code: '339696', line1: 'First Street', day: '1', month: '1', year: '1950', company_line1: 'First Street', company_postal_code: '339696', personal_line1: 'First Street', personal_postal_code: '339696', personal_id_number: '000000000' })
    //   const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
    //   req.account = user.account
    //   req.session = user.session
    //   req.body = {
    //     currency: 'sgd',
    //     country: 'SG',
    //     account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
    //     account_type: 'individual',
    //     account_number: '000123456',
    //     bank_code: '1100',
    //     branch_code: '000'
    //   }
    //   await req.route.api.patch(req)
    //   req.session = await TestHelper.unlockSession(user)
    //   const accountNow = await req.route.api.patch(req)
    //   assert.strictEqual(accountNow.external_accounts.data.length, 1)
    // })

    it(`should reject US invalid fields`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'US', city: 'New York City', personal_city: 'New York City', postal_code: '10001', personal_id_number: '000000000', line1: 'First Street', day: '1', month: '1', year: '1950', company_city: 'New York City', company_state: 'New York', company_line1: 'First Street', company_postal_code: '10001', ssn_last_4: '0000' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'usd',
        country: 'US',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        account_number: '000123456789',
        routing_number: '110000000'
      }
      await testEachFieldAsNull(req)
    })

    it(`should update US information`, async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createStripeAccount(user, { type: 'company', business_tax_id: 1, business_name: user.profile.firstName + '\'s company', country: 'US', city: 'New York City', personal_city: 'New York City', postal_code: '10001', personal_id_number: '000000000', line1: 'First Street', day: '1', month: '1', year: '1950', company_city: 'New York City', company_state: 'New York', company_line1: 'First Street', company_postal_code: '10001', ssn_last_4: '0000' })
      const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        currency: 'usd',
        country: 'US',
        account_holder_name: `${user.profile.firstName} ${user.profile.lastName}`,
        account_type: 'individual',
        account_number: '000123456789',
        routing_number: '110000000'
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      assert.strictEqual(accountNow.external_accounts.data.length, 1)
    })
  })
})
