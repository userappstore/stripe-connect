const dashboard = require('@userappstore/dashboard')
const stripe = require('stripe')()
const stripeCache = require('@userappstore/stripe-subscriptions/src/stripe-cache.js')

// The creation of objects like charges and invoices that happen
// without user actions are indexed as this webhook is notifed.  All
// other types of data are indexed as created by the user.
global.connectWebhooksInProgress = 0

let lastTestNumber
module.exports = {
  auth: false,
  post: async (req, res) => {
    global.connectWebhooksInProgress++
    if (!global.redisClient || global.testEnded) {
      res.statusCode = 200
      endWebhook()
      return res.end()
    }
    let stripeEvent
    try {
      stripeEvent = stripe.webhooks.constructEvent(req.bodyRaw, req.headers['stripe-signature'], process.env.CONNECT_ENDPOINT_SECRET)
    } catch (error) {
      res.statusCode = 500
      endWebhook()
      return res.end()
    }
    if (!stripeEvent) {
      res.statusCode = 500
      endWebhook()
      return res.end()
    }
    if (global.testNumber && stripeEvent.data && stripeEvent.data.object && stripeEvent.data.object.metadata && stripeEvent.data.object.metadata.accountid) {
      const accountid = stripeEvent.data.object.metadata.accountid
      const exists = await global.redisClient.hgetallAsync(accountid)
      if (!exists) {
        res.statusCode = 200
        endWebhook()
        return res.end()
      }
      let appid = stripeEvent.data.object.metadata.appid
      if (appid && appid !== global.appid) {
        res.statusCode = 200
        endWebhook()
        return res.end()
      }
    }
    const add = []
    switch (stripeEvent.type) {
      case 'payout.created':
        const payout = stripeEvent.data.object
        const accountid = await global.redisClient.hgetAsync(`map:stripeid:accountid`, stripeEvent.account)
        await global.redisClient.hsetAsync(`map:payoutid:stripeid`, payout.id, stripeEvent.account)
        add.push({ index: 'payouts', value: payout.id })
        add.push({ index: `bankAccount:payouts:${payout.destination}`, value: payout.id })
        add.push({ index: `account:payouts:${accountid}`, value: payout.id })
        add.push({ index: `stripeAccount:payouts:${stripeEvent.account}`, value: payout.id })
        break
      default:
        if (!stripeEvent.data || !stripeEvent.data.object || !stripeEvent.data.object.id) {
          res.statusCode = 200
          endWebhook()
          return res.end()
        }
        if (stripeEvent.type.endsWith('.updated')) {
          await stripeCache.update(stripeEvent.data.object)
        } else if (stripeEvent.type.endsWith('.deleted')) {
          await stripeCache.delete(stripeEvent.data.object.id)
        }
        break
    }
    if (add.length) {
      for (const item of add) {
        if (!global.redisClient || global.testEnded) {
          res.statusCode = 200
          return res.end()
        }
        await dashboard.RedisList.add(item.index, item.value)
      }
    }
    endWebhook()
    res.statusCode = 200
    return res.end()
  }
}

function endWebhook() {
  return setTimeout(reduceCounter, 200)
}

function reduceCounter() {
  global.connectWebhooksInProgress--
}
