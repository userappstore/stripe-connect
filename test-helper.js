/* eslint-env mocha */
global.applicationPath = __dirname

const dashboard = require('@userappstore/dashboard')
const fs = require('fs')
const path = require('path')
const MetaData = require('./src/meta-data.js')
const stripe = require('stripe')(  )
const stripeCache = require('@userappstore/stripe-subscriptions/src/stripe-cache.js')
const testData = require('@userappstore/dashboard/test-data.json')
const util = require('util')

const stripeKey = {
  api_key: process.env.STRIPE_KEY
}

afterEach((callback) => {
  if (global.connectWebhooksInProgress === 0) {
    return callback()
  }
  function wait() {
    setTimeout(() => {
      if (global.connectWebhooksInProgress === 0) {
        return callback()
      }
      return wait()
    }, 10)
  }
  return wait()
})

const TestHelper = module.exports = dashboard.loadTestHelper()

const createRequestWas = module.exports.createRequest
module.exports.createRequest = (rawURL, method) => {
  const req = createRequestWas(rawURL, method)
  req.stripeKey = stripeKey
  req.userAgent = 'A web browser user agent'
  req.ip = '8.8.8.8'
  req.country = {
    country: {
      iso_code: 'US'
    }
  }
  return req
}

module.exports.createAdditionalOwner = createAdditionalOwner
module.exports.createDocumentUpload = createDocumentUpload
module.exports.createExternalAccount = createExternalAccount
module.exports.createPayout = createPayout
module.exports.createStripeAccount = createStripeAccount
module.exports.submitAdditionalOwners = submitAdditionalOwners
module.exports.submitStripeAccount = submitStripeAccount
module.exports.triggerVerification = triggerVerification
module.exports.uploadFile = uploadFile
module.exports.waitForPayout = util.promisify(waitForPayout)

async function createStripeAccount(user, properties) {
  const req = TestHelper.createRequest(`/api/user/connect/create-stripe-account?accountid=${user.account.accountid}`, 'POST')
  req.session = user.session
  req.account = user.account
  req.body = {
    type: properties.type,
    country: properties.country
  }
  await req.route.api.post(req)
  req.session = await TestHelper.unlockSession(user)
  let stripeAccount = await req.route.api.post(req)
  const req2 = TestHelper.createRequest(`/api/user/connect/update-${properties.type}-registration?stripeid=${stripeAccount.id}`, 'POST')
  req2.session = req.session
  req2.account = req.account
  req2.body = {
    first_name: user.profile.firstName,
    last_name: user.profile.lastName,
    email: user.profile.email
  }
  for (const property in properties) {
    if (property === 'type' || property === 'country') {
      continue
    }
    req2.body[property] = properties[property]
  }
  await req2.route.api.patch(req2)
  req2.session = await TestHelper.unlockSession(user)
  stripeAccount = await req2.route.api.patch(req2)
  user.stripeAccount = stripeAccount
  const req3 = TestHelper.createRequest(`/api/user/connect/country-spec?country=${stripeAccount.country}`, 'GET')
  user.countrySpec = await req3.route.api.get(req3)
  user.session = await dashboard.Session.load(user.session.sessionid)
  if (user.session.lock || user.session.unlocked) {
    throw new Error('session status is locked or unlocked when it should be nothing')
  }
  return stripeAccount
}

async function createExternalAccount(user, details) {
  const req = TestHelper.createRequest(`/api/user/connect/update-payment-information?stripeid=${user.stripeAccount.id}`, 'POST')
  req.session = user.session
  req.account = user.account
  req.body = details
  await req.route.api.patch(req)
  req.session = await TestHelper.unlockSession(user)
  user.stripeAccount = await req.route.api.patch(req)
  user.session = await dashboard.Session.load(user.session.sessionid)
  if (user.session.lock || user.session.unlocked) {
    throw new Error('session status is locked or unlocked when it should be nothing')
  }
  return user.stripeAccount.external_accounts.data[0]
}

async function uploadFile(useFailedFile) {
  const filePath = path.join(__dirname, useFailedFile ? 'test-documentid-failed.png' : 'test-documentid-success.png')
  const uploadData = {
    purpose: 'identity_document',
    file: {
      data: fs.readFileSync(filePath),
      name: useFailedFile ? 'failed.png' : 'success.png',
      type: 'application/octet-stream'
    }
  }
  const file = await stripe.files.create(uploadData, stripeKey)
  return file
}

async function createDocumentUpload(user, useFailedFile) {
  const file = await uploadFile(useFailedFile)
  const registration = MetaData.parse(user.stripeAccount.metadata, 'registration') || {}
  registration.document = file.id
  const accountInfo = {
    metadata: {}
  }
  MetaData.store(accountInfo.metadata, 'registration', registration)
  user.stripeAccount = await stripe.accounts.update(user.stripeAccount.id, accountInfo, stripeKey)
  await stripeCache.update(user.stripeAccount)
}

let testDataIndex = 0
async function createAdditionalOwner(user, properties) {
  testDataIndex++
  const req = TestHelper.createRequest(`/api/user/connect/create-additional-owner?stripeid=${user.stripeAccount.id}`, 'POST')
  req.session = user.session
  req.account = user.account
  req.body = {
    first_name: testData[testDataIndex].firstName,
    last_name: testData[testDataIndex].lastName,
    email: testData[testDataIndex].email
  }
  if (properties) {
    for (const field in properties) {
      req.body[field] = properties[field]
    }
  }
  await req.route.api.post(req)
  req.session = await TestHelper.unlockSession(user)
  const owner = await req.route.api.post(req)
  user.session = await dashboard.Session.load(user.session.sessionid)
  user.owner = owner
  return owner
}

let payoutAmount = 0
async function createPayout(user) {
  payoutAmount += 100
  // the Stripe API has to be used here directly because this module
  // assumes payouts will be handled automatically so there aren't
  // any API endpoints to create payouts
  const accountKey = {
    api_key: stripeKey.api_key,
    stripe_account: user.stripeAccount.id
  }
  const chargeInfo = {
    amount: payoutAmount * 2,
    currency: 'usd',
    source: 'tok_bypassPending',
    description: 'Test charge'
  }
  await stripe.charges.create(chargeInfo, accountKey)
  const payoutInfo = {
    amount: payoutAmount,
    currency: 'usd',
    metadata: {
      testNumber: global.testNumber
    }
  }
  const payout = await stripe.payouts.create(payoutInfo, accountKey)
  user.payout = payout
  return payout
}

async function submitAdditionalOwners(user) {
  const req = TestHelper.createRequest(`/api/user/connect/set-additional-owners-submitted?stripeid=${user.stripeAccount.id}`, 'POST')
  req.session = user.session
  req.account = user.account
  await req.route.api.patch(req)
  req.session = await TestHelper.unlockSession(user)
  const stripeAccount = await req.route.api.patch(req)
  user.stripeAccount = stripeAccount
  user.session = await dashboard.Session.load(user.session.sessionid)
  if (user.session.lock || user.session.unlocked) {
    throw new Error('session status is locked or unlocked when it should be nothing')
  }
  return stripeAccount
}

async function submitStripeAccount(user) {
  const req = TestHelper.createRequest(`/api/user/connect/set-${user.stripeAccount.legal_entity.type}-registration-submitted?stripeid=${user.stripeAccount.id}`, 'POST')
  req.session = user.session
  req.account = user.account
  await req.route.api.patch(req)
  req.session = await TestHelper.unlockSession(user)
  const stripeAccount = await req.route.api.patch(req)
  user.stripeAccount = stripeAccount
  user.session = await dashboard.Session.load(user.session.sessionid)
  if (user.session.lock || user.session.unlocked) {
    throw new Error('session status is locked or unlocked when it should be nothing')
  }
  return stripeAccount
}

async function triggerVerification(user) {
  const accountKey = {
    api_key: stripeKey.api_key,
    stripe_account: user.stripeAccount.id
  }
  const chargeInfo = {
    amount: 2000,
    currency: 'usd',
    source: 'tok_visa_triggerVerification',
    description: 'Test charge'
  }
  const charge = await stripe.charges.create(chargeInfo, accountKey)
  user.charge = charge
  return charge
}

async function waitForPayout(stripeid, previousid, callback) {
  callback = callback || previousid
  if (callback === previousid) {
    previousid = null
  }
  async function wait() {
    if (!global.redisClient || global.testEnded) {
      return
    }
    const itemids = await dashboard.RedisList.list(`${global.appid}:stripeAccount:payouts:${stripeid}`, 0, 1)
    if (!itemids || !itemids.length) {
      return setTimeout(wait, 10)
    }
    if (previousid && previousid === itemids[0]) {
      return setTimeout(wait, 10)
    }

    // give time for a webhook to be completely processed
    function waitForWebHookCompletion() {
      if (global.subscriptionWebhooksInProgress > 0) {
        return setTimeout(waitForWebHookCompletion, 10)
      }
      return callback(null, itemids[0])
    }
    return setTimeout(waitForWebHookCompletion, 10)
  }
  return setTimeout(wait, 10)
}
